function [s0, CurrFrmResd] = RPE_frame_decoder(FrmBitStrm, PrevFrmResd)
% Function that implements inverse short and long term filtering and then processing
%
% Inputs:
%               - FrmBitStrm: Current frame bit stream
%               - PrevFrmResd: residual values of previous frame (d')
% Outputs :
%               - s0: final signal
%               - CurrFrmResd: residual values of current frame (d')
%

[LARc,Nc,bc,Mc,x_max_c,x_ratios_c] = decompose_frame(FrmBitStrm); % Derive parameters from bit stream

%% Derive prediction polynomial from Log-Area ratios

LAR_decoded = lar_dec(LARc);
r_decoded = lar2rc(LAR_decoded);
a_decoded = rc2poly(r_decoded);

%% Derive signal from bit stream parameters

for CurrSubFrm = 0:3
    d = subfunctionD(PrevFrmResd,bc(CurrSubFrm+1),Nc(CurrSubFrm+1),x_ratios_c(CurrSubFrm+1,:),x_max_c(CurrSubFrm+1),Mc(CurrSubFrm+1)); % Perform subfunction D
    PrevFrmResd = [PrevFrmResd(41:160); d]; % Update previous subframes residual values (d')
end

CurrFrmResd = PrevFrmResd; % Update current frame residual (d')

s0 = filter(1,a_decoded,CurrFrmResd); % Signal filtering
s0 = post_processing(s0); % Post processing

end

