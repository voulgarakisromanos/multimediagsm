function [LARc,Nc,bc,Mc,xmaxc,xMc] = decompose_frame(FrmBitStrm)
% Turn bit stream to frame parameters

LARc = zeros(8,1);
Nc = zeros(4,1);
bc = zeros(4,1);
Mc = zeros(4,1);
xmaxc = zeros(4,1);
xMc = zeros(4,13);

LARc(1) = bi2de(FrmBitStrm(1:5)');
if (FrmBitStrm(6)==1)
    LARc(1) = -LARc(1);
end
LARc(2) = bi2de(FrmBitStrm(7:11)');
if (FrmBitStrm(12)==1)
    LARc(2) = -LARc(2);
end
LARc(3) = bi2de(FrmBitStrm(13:16)');
if (FrmBitStrm(17)==1)
    LARc(3) = -LARc(3);
end
LARc(4) = bi2de(FrmBitStrm(18:21)');
if (FrmBitStrm(22)==1)
    LARc(4) = -LARc(4);
end
LARc(5) = bi2de(FrmBitStrm(23:25)');
if (FrmBitStrm(26)==1)
    LARc(5) = -LARc(5);
end
LARc(6) = bi2de(FrmBitStrm(27:29)');
if (FrmBitStrm(30)==1)
    LARc(6) = -LARc(6);
end
LARc(7) = bi2de(FrmBitStrm(31:32)');
if (FrmBitStrm(33)==1)
    LARc(7) = -LARc(7);
end
LARc(8) = bi2de(FrmBitStrm(34:35)');
if (FrmBitStrm(36)==1)
    LARc(8) = -LARc(8);
end
Nc(1) = bi2de(FrmBitStrm(37:43)');
bc(1) = bi2de(FrmBitStrm(44:45)');
Mc(1) = bi2de(FrmBitStrm(46:47)');
xmaxc(1) = bi2de(FrmBitStrm(48:53)');
for i = 1:13
    xMc(1,i) = bi2de(FrmBitStrm(54+3*(i-1):56+3*(i-1))');
end
Nc(2) = bi2de(FrmBitStrm(93:99)');
bc(2) = bi2de(FrmBitStrm(100:101)');
Mc(2) = bi2de(FrmBitStrm(102:103)');
xmaxc(2) = bi2de(FrmBitStrm(104:109)');
for i = 1:13
    xMc(2,i) = bi2de(FrmBitStrm(110+3*(i-1):112+3*(i-1))');
end
Nc(3) = bi2de(FrmBitStrm(149:155)');
bc(3) = bi2de(FrmBitStrm(156:157)');
Mc(3) = bi2de(FrmBitStrm(158:159)');
xmaxc(3) = bi2de(FrmBitStrm(160:165)');
for i = 1:13
    xMc(3,i) = bi2de(FrmBitStrm(166+3*(i-1):168+3*(i-1))');
end
Nc(4) = bi2de(FrmBitStrm(205:211)');
bc(4) = bi2de(FrmBitStrm(212:213)');
Mc(4) = bi2de(FrmBitStrm(214:215)');
xmaxc(4) = bi2de(FrmBitStrm(216:221)');
for i = 1:13
    xMc(4,i) = bi2de(FrmBitStrm(222+3*(i-1):224+3*(i-1))');
end

QLB = [0.1,0.35,0.65,1];  % Quantizing level    

for i=1:4
    if (bc(i)==0)
        bc(i) = QLB(1);
    elseif (bc(i)==1)
        bc(i) = QLB(2);
    elseif (bc(i)==2)
        bc(i) = QLB(3);
    else
        bc(i) = QLB(4);
    end


end

