function bitarray = decimal2binary(number,bits)
% Function that derives bit array from decimal number. If the number is
% negative the last bit becomes 1
bitarray = zeros(1,bits);
if (number<0)
    bitarray(bits) = 1;
    number = abs(number);
end
for i=2:bits-1
    if (number >= 2^(bits-i))
        bitarray(bits-i+1) = 1;
        number = number - 2^(bits-i);
    end
end
if (number ==1)
    bitarray(1) = 1;
end
end

