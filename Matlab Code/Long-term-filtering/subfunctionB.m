function e = subfunctionB(CurrSubFrmPredError,bc,Nc,PrevFrmResd)
%   Calculates the long term prediction error (e)
%   
%   Inputs: 
%               - d: current subframe short term prediction error (d)
%               - bc,Nc: quantized LTP parameters
%               - PrevFrmResd: previous frame residual (d')
%   Outputs:
%               - e: current subframe long term prediction error (e)

e = zeros(40,1);
for n = 1:40
    e(n) = CurrSubFrmPredError(n) - bc*PrevFrmResd(n-Nc+160); % e(n) = d(n) - b'*d'(n-N')
end

