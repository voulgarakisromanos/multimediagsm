function [x_max_c,x_ratios_c] = apcm_quantization(x_max,x_ratios)
%APCM_QUANTIZATION Summary of this function goes here
%   Detailed explanation goes here

loadfile = load('xmax_quant.mat');
table = struct2cell(loadfile);
quant_table = cell2mat(table);

for i = 1:length(quant_table)
    if (x_max<=quant_table(i,1))
        x_max_c = quant_table(i,2);
        break;
    end
end


x_ratios_mat = [-32768,-24577,0;
                    -24576,-16385,1;
                    -16384,-8193,2;
                    -8192,-1,3;
                    0,8191,4;
                    8192,16383,5;
                    16384,24575,6;
                    24576,32767,7];
                    
x_ratios_c= zeros(length(x_ratios),1);
for i = 1:length(x_ratios)
    for j = 1:length(x_ratios_mat)
        if ((x_ratios(i)>x_ratios_mat(j,1)) && (x_ratios(i)<=x_ratios_mat(j,2)))
            x_ratios_c(i) = x_ratios_mat(j,3);
        end
    end
end

end

