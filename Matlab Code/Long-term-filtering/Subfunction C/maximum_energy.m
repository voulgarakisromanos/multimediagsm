function Mc = maximum_energy(x_matrix)
% Function that calculates the signal with the most energy and returns its index

energy = zeros(4,1);

for i = 1:4
    energy(i) = 0;
    for j = 1:length(x_matrix)
        energy(i) = energy(i) + x_matrix(i,j)^2;
    end    
end

[~,index] = max(energy);
Mc = index - 1;
end

