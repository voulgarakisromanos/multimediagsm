function x = weighting_filter(e)
%WEIGHTING_FILTER Summary of this function goes here
%   Detailed explanation goes here
H = [-134 -374 0 2054 5741 8192 5741 2054 0 -374 -134];
x = zeros(40,1);

for k = 0:39
    for i = 0:10
        if ((k+5-i)>1 && (k+5-i)<40)
            x(k+1) = x(k+1) + H(i+1)*e(k+5-i+1);
        end
    end
end

