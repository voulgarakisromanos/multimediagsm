function [x_ratios_c,x_max_c,Mc] = subfunctionC(e)
% Function that implements sampling and quantizing of long term prediction error (e)
% 
% Inputs :
%               - e: long term prediction error (e)
% Outputs:
%               - x_ratios_c: RPE pulses
%               - x_mac_x: block amplitude
%               - Mc: RPE grid position
% 

x = weighting_filter(e); % Apply weightinh filter

%% Construct x matrix
x_matrix = zeros(4,13);
for i = 1:13
    for j = 1:4
        x_matrix(j,i) = x(j+(3*i-3));
    end
end

%% Calculation and quantizing of RPE parameters

Mc = maximum_energy(x_matrix);
x_max = max(abs(x_matrix(Mc+1,:)));
x_ratios = x_matrix(Mc+1,:)/x_max;
[x_max_c,x_ratios_c] = apcm_quantization(x_max,x_ratios);

end

