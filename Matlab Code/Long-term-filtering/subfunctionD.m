function  CurrSubFrmResd = subfunctionD(PrevFrmResd,bc,Nc,x_ratios_c,x_max_c,Mc)
% Function responsible for residual sequence calculation (d')
% 
% Inputs :
%               - s0: original signal
%               - PrevFrmResd: residual values of previous frame (d')
% Outputs:
%               - FrmBitStrm: 260 bit stream
%               - CurrFrmResd: residual values of current frame (d')
% 

CurrSubFrmResd = zeros(40,1);
loadfile = load('xmax_quant.mat');
table = struct2cell(loadfile);
quant_table = cell2mat(table);

%% Dequantization of RPE parameters

for i = 1:length(quant_table)
    if (x_max_c == quant_table(i,2))
        x_max = quant_table(i,1)/2^15;
    end
end

ratios_quant_table = [-28672,0;
                              -20480,1;
                              -12288,2;
                              -4096,3;
                              4096,4;
                              12288,5;
                              20480,6;
                              28672,7];
x_n= zeros(13,1);       
for i = 1:13
    for j = 1:7
        if(x_ratios_c(i)==ratios_quant_table(j,2))
            x_n(i)=ratios_quant_table(j,1)/2^15;
        end
    end
end

x_n = x_n*x_max;

%% Calculation of e'(n)

e_n = zeros(40,1);
for i = 1:13
    e_n(Mc+1+3*(i-1)) = x_n(i);
end

%% Calculate d'(n) = e'(n) +b'*d'(n-N')

for n = 1:40
    CurrSubFrmResd(n) = e_n(n) + bc*PrevFrmResd(n-Nc+160); 
end

end

