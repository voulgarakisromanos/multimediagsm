function [N,b] = subfunctionA(d,PrevFrmResd)
% Function that estimates the LTP parameters N,b
%
% Outputs:
%               - quantized LTP parameters N,b
% Inputs: 
%               - d: current subframe short term prediction error (d)
%               - PrevFrmResd: previous frame residual (d')

R_matrix = zeros(80,1);
DLB = [0.2,0.5,0.8];        % Decision level
QLB = [0.1,0.35,0.65,1];  % Quantizing level    
    
%% Calculate R matrix values

    for lambda = 41:120
        for i = 1:40
                R_matrix(lambda-40) = R_matrix(lambda-40) + d(i)*PrevFrmResd(i-lambda+160);
        end
    end
    
%% Calculate N

    [~,I] = max(R_matrix);
    N = I + 40;
    
%% Calculate b

    if (R_matrix(I)==0)
        b=0;
    else
        sum = 0;
        for i = 1:40
                sum = sum + PrevFrmResd(i-N+160)^2;
        end
        b = R_matrix(I)/sum;
    end
    
%% Coding of b

    if (b<=DLB(1))
        b = 0;
    elseif (b>DLB(1) && b<=DLB(2))
        b = 1;
    elseif (b>DLB(2) && b<=DLB(3))
        b = 2;
    else
        b = 3;
    end
    
%% Decoding of b

    if (b==0)
        b = QLB(1);
    elseif (b==1)
        b = QLB(2);
    elseif (b==2)
        b = QLB(3);
    else
        b = QLB(4);
    end
end


