function [FrmBitStrm,CurrFrmResd] = RPE_frame_coder(s0, PrevFrmResd)
% Function that after preprocessing of the signal performs short and long
% term filtering
% 
% Inputs :
%               - s0: original signal
%               - PrevFrmResd: residual values of previous frame (d')
% Outputs:
%               - FrmBitStrm: 260 bit stream
%               - CurrFrmResd: residual values of current frame (d')
% 

%% Initialize matrices

x_ratios_c_matrix = zeros(4,13);
x_max_c_matrix = zeros(4,1);
Mc_matrix = zeros(4,1);
b_matrix = zeros(4,1);
N_matrix = zeros(4,1);
CurrFrmExFull = zeros(160,1);

%% SLT coder

[LARc,prediction_error] = RPE_frame_ST_coder(s0); % Preprocessing and short term filtering

for CurrSubFrm = 0:3
    
        CurrSubFrmPredError = prediction_error(40*CurrSubFrm+1:40*(CurrSubFrm+1)); % Get current subframe short term prediction error (d)
        [Nc,bc] = subfunctionA(CurrSubFrmPredError,PrevFrmResd); % Perform subfunction A
        CurrSubFrmExFull = subfunctionB(CurrSubFrmPredError,bc,Nc,PrevFrmResd); % Perform subfunction B
        [x_ratios_c,x_max_c,Mc] = subfunctionC(CurrSubFrmExFull); % Perform subfunction C
        CurrSubFrmResd = subfunctionD(PrevFrmResd,bc,Nc,x_ratios_c,x_max_c,Mc); % Perform subfunction D
        PrevFrmResd = [PrevFrmResd(41:160); CurrSubFrmResd]; %% Update previous subframes residual values (d')
        
        %% Update matrices
        
        CurrFrmExFull(40*CurrSubFrm+1:40*(CurrSubFrm+1)) = CurrSubFrmExFull;
        Mc_matrix(CurrSubFrm+1) = Mc;
        x_ratios_c_matrix(CurrSubFrm+1,:) = x_ratios_c;
        x_max_c_matrix(CurrSubFrm+1) = x_max_c;
        N_matrix(CurrSubFrm+1) = Nc;
        b_matrix(CurrSubFrm+1) = bc;
        
end
FrmBitStrm = compose_frame(LARc,N_matrix,b_matrix,Mc_matrix,x_max_c_matrix,x_ratios_c_matrix); % Derive frame bit stream
CurrFrmResd = PrevFrmResd; % Update current frame residual (d')
%%
end

