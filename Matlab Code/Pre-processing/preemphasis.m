function [s] = preemphasis(s_of)
% Function to implement Pre-emphasis

    beta = 28180 * 2^(-15);
    s = zeros(length(s_of),1);
    s(1) = 0;
    for i=2:length(s_of)
        s(i) = s_of(i) - beta*s_of(i-1);
    end
    
end

