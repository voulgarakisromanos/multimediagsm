function [s_of] = offset_compensation(s)
% Function to implement offset compensation

    alpha = 32735 * 2^(-15);
    s_of = zeros(length(s),1);
    s_of(1) = 0;
    for i=2:length(s_of)
        s_of(i) = s(i) - s(i-1) + alpha*s_of(i-1);
    end
    
end

