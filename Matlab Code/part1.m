clear all;
close all;


x = input('Select signal to decode (1-3):\n');
if (x==1)
    str = 'original_ex_1.wav';
elseif (x==2)
    str = 'original_ex_2.wav';
else
    str = 'original_ex_3.wav';
end
[y,~] = audioread(str);
audio = zeros(length(y),1);

%% Main loop

for i = 0:length(y)/160-1
%% Encoder

    s0 = y(160*i+1:160*(i+1));       % Get current frame
    [LARc,CurrFrmResd] = RPE_frame_ST_coder(s0);    
    
%% Decoder

    s0 = RPE_frame_ST_decoder(LARc,CurrFrmResd);
    audio(i*160+1:(i+1)*160) = s0;
    
end
%%
audiowrite('example1.wav',audio,8000);

