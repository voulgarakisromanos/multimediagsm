function [sro] = post_processing(sr)
% Function that performs post processing of the signal

beta = 28180*2^(-15);
sro = zeros(length(sr),1);
sro(1) = sr(1);
for k = 2:length(sr)
    sro(k) = sr(k) + beta*sro(k-1);
end
end

