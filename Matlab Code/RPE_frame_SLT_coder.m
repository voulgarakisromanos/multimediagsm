function [LARc,N_matrix,b_matrix,CurrFrmExFull,CurrFrmResd] = RPE_frame_SLT_coder(s0, PrevFrmResd)
% Function that calls RPE_frame_ST_coder and additionally implements long term filtering
% 
% Inputs :
%               - s0: original signal
%               - PrevFrmResd: residual values of previous frame (d')
% Outputs:
%               - LARc: quantized Log-Area ratios
%               - N_matrix: 4x1 matrix containing Nc values for 4 subframes
%               - b_matrix: 4x1 matrix containing bc values for 4 subframes
%               - CurrFrmExFull: prediction error (e)
%               - CurrFrmResd: residual values of current frame (d')

[LARc,d] = RPE_frame_ST_coder(s0); % Preprocessing and short term filtering

%% Initialize matrices
N_matrix = zeros(4,1);
b_matrix = zeros(4,1);
CurrFrmExFull = zeros(160,1);
CurrSubFrmResd = zeros(40,1);
%% 

for CurrSubFrm = 0:3
        CurrSubFrmPredError = d(40*CurrSubFrm+1:40*(CurrSubFrm+1)); % Get current subframe short term prediction error (d)
        [Nc,bc] = subfunctionA(CurrSubFrmPredError,PrevFrmResd); % Perform subfunction A
        CurrSubFrmExFull = subfunctionB(CurrSubFrmPredError,bc,Nc,PrevFrmResd); % Perform subfunction B
        %% Subfunction D
        for n = 1:40
            CurrSubFrmResd(n) = CurrSubFrmExFull(n) + bc*PrevFrmResd(n-Nc+160); % d'(n) = e(n) + b'*d'(n-N')
        end
        %%
        PrevFrmResd = [PrevFrmResd(41:160); CurrSubFrmResd]; % Update previous subframes residual values (d')
        %% Update matrices
        CurrFrmExFull(40*CurrSubFrm+1:40*(CurrSubFrm+1)) = CurrSubFrmExFull; 
        N_matrix(CurrSubFrm+1) = Nc;
        b_matrix(CurrSubFrm+1) = bc;
        %%
end
CurrFrmResd = PrevFrmResd; % Update current frame residual (d')
end

