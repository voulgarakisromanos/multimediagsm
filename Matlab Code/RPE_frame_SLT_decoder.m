function [s0, CurrFrmResd] = RPE_frame_SLT_decoder(LARc,Nc,bc,CurrFrmExFull, PrevFrmResd)
% Function that implements inverse short and long term filtering and then processing
%
% Inputs:
%               - LARc: quantized Log-Area ratios
%               - Nc: 4x1 matrix containing LTP lag values for 4 subframes
%               - bc: 4x1 matrix containing LTP gain values for 4 subframes
%               - CurrFrmExFull: long term prediction error (e)
%               - PrevFrmResd: residual values of previous frame (d')
% Outputs :
%               - s0: final signal
%               - CurrFrmResd: residual values of current frame (d')

CurrSubFrmResd = zeros(40,1); % Current subframe residual values (d')

%% Derive prediction polynomial from Log-Area ratios

LAR_decoded = lar_dec(LARc);
r_decoded = lar2rc(LAR_decoded);
a_decoded = rc2poly(r_decoded);

%% Derive signal from d'(n) = e(n) + b'*d'(n-N')

for j = 0:3
    for n = 1:40
        CurrSubFrmResd(n) = CurrFrmExFull(40*j + n) + bc(j+1)*PrevFrmResd(n-Nc(j+1)+160); % d'(n) = e(n) + b'*d'(n-N')
    end
    PrevFrmResd = [PrevFrmResd(41:160);CurrSubFrmResd]; % Update previous subframes residual values (d')
end

CurrFrmResd = PrevFrmResd; % Update current frame residual (d')

s0 = filter(1,a_decoded,CurrFrmResd); % Signal filtering
s0 = post_processing(s0); % Post processing


end

