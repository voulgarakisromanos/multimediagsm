function [LARc,CurrFrmResd] = RPE_frame_ST_coder(s0)
% Function that implements preprocessing of the signal
% and short term filtering 

%% Preprocessing
s_of = offset_compensation(s0);
s = preemphasis(s_of);
%% Short term fitlering

[rs,R] = ACF(s);              % Estimate autocorrelation
r = rs(2:9);

if (norm(r) == 0)
    w = zeros(8,1);
else
    w = R\r;                         % Solve Rw=r
end

r = poly2rc([1; -w]);
LAR = rc2lar(r);                   % Calculate  Log-Area Ratios
LARc = lar_quant(LAR);         % Quantization and coding of Log.-Area Ratios

LAR_decoded = lar_dec(LARc);
r_decoded = lar2rc(LAR_decoded);
a_decoded = rc2poly(r_decoded);

CurrFrmResd = prediction_error(s,a_decoded); % Calculate  Prediction error (d)


end

