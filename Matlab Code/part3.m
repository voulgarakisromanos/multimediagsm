clear all;
close all;

x = input('Select signal to decode (1-3):\n');
if (x==1)
    str = 'original_ex_1.wav';
elseif (x==2)
    str = 'original_ex_2.wav';
else
    str = 'original_ex_3.wav';
end
[y,~] = audioread(str);
audio = zeros(length(y),1);

PrevFrmResd = zeros(160,1);
LARc = zeros(floor(length(y)/160),8);
Nc = zeros(floor(length(y)/160),4);
bc = zeros(floor(length(y)/160),4);
FrmBitStrm = zeros(260,floor(length(y)/160));
x_ratios_c = zeros(4,13,floor(length(y)/160));
x_max_c = zeros(4,floor(length(y)/160));
Mc = zeros(4,floor(length(y)/160));

for i = 1:length(y)/160
%% Encoder
    s0 = y(160*(i-1)+1:160*i);       % Get current frame
    [FrmBitStrm(:,i),CurrFrmResd] = RPE_frame_coder(s0, PrevFrmResd); 
    PrevFrmResd = CurrFrmResd;
end
PrevFrmResd = zeros(160,1);
for i = 1:length(y)/160
%% Decoder
    [s0, CurrFrmResd] = RPE_frame_decoder(FrmBitStrm(:,i), PrevFrmResd);
    PrevFrmResd = CurrFrmResd;
    audio(i*160+1:(i+1)*160) = s0;
end

audiowrite('example3.wav',audio,8000);

