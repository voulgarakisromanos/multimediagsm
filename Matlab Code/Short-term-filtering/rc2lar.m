function LAR = rc2lar(r)
% Computes the Log-Area Ratios, LAR, based on the
% reflection coefficients, r.

LAR = zeros(8,1);

for i=1:8
    if (abs(r(i))<0.675)
        LAR(i) = r(i);
    elseif (0.675 <= abs(r(i)) && abs(r(i))<0.950)
        LAR(i) = sign(r(i))*(2*abs(r(i))-0.675);
    else
        LAR(i) = sign(r(i))*(8*abs(r(i))-6.375);
    end

end

