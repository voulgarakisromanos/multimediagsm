function [r,R] = ACF(s)
% ACF Calculates the first 9 estimates of the autocorrelation r(k) 
%   r = ACF(s) returns a matrix with the first 9 estimates
%   [r,R] = ACF(s,k) returns the correlation matrix R
% 
%   s is a matrix containing 160 samples

r = zeros(9,1);
for k=0:8
    sum = 0;
    for i=(k+1):160
        sum = sum + s(i)*s(i-k);
    end
    r(k+1) = sum;
end

R = [r(1)   r(2)   r(3)   r(4)   r(5)   r(6)   r(7)   r(8);
      r(2)   r(1)   r(2)   r(3)   r(4)   r(5)   r(6)   r(7); 
      r(3)   r(2)   r(1)   r(2)   r(3)   r(4)   r(5)   r(6);
      r(4)   r(3)   r(2)   r(1)   r(2)   r(3)   r(4)   r(5);
      r(5)   r(4)   r(3)   r(2)   r(1)   r(2)   r(3)   r(4);
      r(6)   r(5)   r(4)   r(3)   r(2)   r(1)   r(2)   r(3);
      r(7)   r(6)   r(5)   r(4)   r(3)   r(2)   r(1)   r(2);
      r(8)   r(7)   r(6)   r(5)   r(4)   r(3)   r(2)   r(1)];
  
end

