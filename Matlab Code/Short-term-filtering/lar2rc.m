function r = lar2rc(LAR)
% Computes the reflection coefficients, r, based on the
% Log-Area Ratios, LAR.

r = zeros(8,1);

for i=1:8
    if (abs(LAR(i))<0.675)
        r(i) = LAR(i);
    elseif (0.675 <= abs(LAR(i)) && abs(LAR(i))<1.225)
        r(i) = sign(LAR(i))*(0.5*abs(LAR(i)) + 0.3375);
    else
        r(i) = sign(LAR(i))*(0.125*abs(LAR(i)) + 0.796875);
    end
end


end

