function d = prediction_error(s,a)
% Function to calculate the short term prediction error (d) based on
% prediction polynomial (a).
% 
% Inputs:
%               - s: original signal
%               - a: prediction polynomial (a)
% Outputs :
%               - d: short term prediction error
% 

s_hat = zeros(160,1);

for i = 1:160
    for k=2:9
        if (i-k-1>0)
            s_hat(i) = a(k)*s(i-k+1); 
        end
    end
end

d = s - s_hat;

end

