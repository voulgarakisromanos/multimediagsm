function s0 = RPE_frame_ST_decoder(LARc,d)
% Function that inverts the short term filtering and performs post
% processing

s0 = zeros(160,1);

%% Inverse short term filtering

LAR_decoded = lar_dec(LARc);
r_decoded = lar2rc(LAR_decoded);
a_decoded = rc2poly(r_decoded);

s0 = filter(1,a_decoded,d);
%% Post processing

s0 = post_processing(s0);

end

